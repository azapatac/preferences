﻿namespace Preferences
{
    using System.Threading.Tasks;
    using Xamarin.Forms;

    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Xamarin.Essentials.Preferences.ContainsKey("Email"))
            {
                email.Text = Xamarin.Essentials.Preferences.Get("Email", string.Empty);
            }

            if (Xamarin.Essentials.Preferences.ContainsKey("Password"))
            {
                password.Text = Xamarin.Essentials.Preferences.Get("Password", string.Empty);
            }

            remember.IsChecked = string.IsNullOrEmpty(email.Text) == false || string.IsNullOrEmpty(password.Text) == false;
        }

        async void Button_Clicked(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(email.Text))
            {
                await App.Current.MainPage.DisplayAlert("Preferences", "Email required", "OK");
                return;
            }

            if (string.IsNullOrEmpty(password.Text))
            {
                await App.Current.MainPage.DisplayAlert("Preferences", "Password required", "OK");
                return;
            }

            if (remember.IsChecked)
            {
                Xamarin.Essentials.Preferences.Set("Email", email.Text);
                Xamarin.Essentials.Preferences.Set("Password", password.Text);
            }
            else
            {
                Xamarin.Essentials.Preferences.Remove("Email");

                Xamarin.Essentials.Preferences.Clear();
            }

            indicator.IsRunning = true;
            content.IsEnabled = false;
            await Task.Delay(3000);
            content.IsEnabled = true;
            indicator.IsRunning = false;

            await App.Current.MainPage.DisplayAlert("Preferences", "User not found", "OK");
        }
    }
}